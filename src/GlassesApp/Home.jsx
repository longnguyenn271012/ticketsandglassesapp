import React, { Component } from 'react';
import Model from './Model';
import ProductList from './ProductList';

class Home extends Component {
    render() {
        return (
            <div className="container-fluid" id="home"
                style={{
                    backgroundImage: "linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(./assets/glassesImage/background.jpg)",
                    backgroundSize: "cover",
                }}>
                <div className="container-fluid text-center text-white py-4"
                    style={{
                        background: "linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6))",
                    }}>
                    <h1>TRY GLASSES APP ONLINE</h1>
                </div>
                <div>
                    <Model />
                    <ProductList />
                </div>
            </div>
        );
    }
}

export default Home;