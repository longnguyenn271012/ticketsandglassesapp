import React, { Component } from 'react'
import { connect } from 'react-redux'

class Model extends Component {
    render() {
        const { name, url, desc } = this.props.selectedProduct;
        return (
            <div className="container my-5" 
            style={{
                background: "url(./assets/glassesImage/model.jpg)",
                backgroundSize: "cover",
                height: "400px",
                width: "300px",
                position: "relative",
            }}>
                <div style={{
                    backgroundImage:`url(${url})`,
                    backgroundSize: "cantain",
                    backgroundRepeat: "no-repeat",
                    width: "450px",
                    height: "150px",
                    position: "absolute",
                    top: "60px",
                    left: "-47px",
                    transform: "scale(0.48)",
                    opacity: "0.6",
                }}/>
                <div className="content" 
                style={Object.keys(this.props.selectedProduct).length !== 0 
                    ? {
                    backgroundColor: "rgba(250, 140, 0, 0.5)",
                    position:"absolute",
                    bottom:"0", 
                    left: "0",
                    paddingLeft: "10px"
                    } : null
                }>
                    <h3 style={{color:"#9900cc"}}>{name}</h3>
                    <p>{desc}</p>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        selectedProduct: state.products.selectedProduct
    }
}

export default connect(mapStateToProps)(Model);
