import React, { Component } from 'react';
import GheItem from './GheItem';

class DanhSachGhe extends Component {
    render() {
        return (
            <div>
                <div className="container">
                    <h5 className="bg-light text-center py-3 fs-5">Tài Xế</h5>
                        <GheItem
                            seats = {this.props.seats}
                            addToBookingList = {this.props.addToBookingList}
                        />
                </div>
            </div>
        );
    }
}

export default DanhSachGhe;