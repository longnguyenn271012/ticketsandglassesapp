import React, { Component } from 'react';

class GheItem extends Component {
    render() {
        const renderSeatsHTML = () => {
            const seatsHTML = this.props.seats.map((item) => {
        
                if (item.TrangThai) {
                    return (
                        <div className="col-3 my-2">
                            <button className="btn btn-danger" disabled>{item.SoGhe}</button>
                        </div>
                    )
                } else {
                    return (
                        <div className="col-3 my-2">
                            <button className={item.isBooking ? "btn btn-success" : "btn btn-secondary"}
                                
                                onClick={() => this.props.addToBookingList(item)}> 
                                {item.SoGhe} 
                            </button> 
                        </div>
                    )
                }
            })
                return seatsHTML;
            }
            return (
                <div className="row">
                    {renderSeatsHTML()}
                </div >
            )
        }
    
}
export default GheItem;