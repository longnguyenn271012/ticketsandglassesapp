import React, { Component } from 'react';

class DanhSachGheDangDat extends Component {
    renderBookingList = () => {
        const bookingListHTML = this.props.bookingList.map((item) => {
            return (
                <div>
                    <p className="fs-5">
                        Ghế: {item.TenGhe} ${item.Gia}
                        <a href="#" className="text-danger ml-2">[Hủy]</a>
                    </p>
                </div>
            )
        });
        return bookingListHTML;
    }
    render() {
        return (
            <div className="container">
                <h4 className="text-center text-warning py-3">Danh Sách Ghế Đang Đặt ({this.props.bookingList.length})</h4>
                {this.renderBookingList()}
            </div>
        )
    }

    
}

export default DanhSachGheDangDat;